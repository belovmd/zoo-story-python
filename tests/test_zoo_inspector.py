from unittest import TestCase
from unittest.mock import ANY
from unittest.mock import Mock

from src.zoo_inspector import ZooInspector


class InspectionLogStub(object):
    def log(self, statuses):
        self.statuses = statuses

    def get_statuses(self):
        return self.statuses


class TestZooInspector(TestCase):
    def setUp(self):
        self.zoo_id = 'zoo'
        self.test_enclosure_id = 'enclosure'
        self.test_animal_name = 'animal'

        self.zoo = Mock()
        self.zoo.get_id.return_value = self.zoo_id

        self.image_recognition_system = Mock()
        self.inspection_log = InspectionLogStub()
        self.zoo_inspector = ZooInspector(self.image_recognition_system, self.inspection_log)

    def verify_log(self, *statuses):
        expected_statuses = statuses
        actual_statuses = self.inspection_log.get_statuses()

        self.assertTupleEqual(expected_statuses, tuple(actual_statuses))

    def test_when_no_animals_then_no_requests(self):
        self.zoo.get_enclosures.return_value = []

        self.zoo_inspector.inspect(self.zoo)

        self.zoo.request_maintenance_crew_to.assert_not_called()
        self.zoo.request_security_to.assert_not_called()
        self.zoo.request_veterinary_to.assert_not_called()
        self.zoo.close_enclosure.assert_not_called()

        self.verify_log(f'ZOO#{self.zoo_id}#OK')

    def test_when_enclosure_and_animal_alright_then_no_requests(self):
        animal = Mock()

        enclosure = Mock()
        enclosure.get_animal.return_value = animal

        animal_status = Mock()
        animal_status.is_animal_sick.return_value = False

        enclosure_status = Mock()
        enclosure_status.is_enclosure_safe.return_value = True

        self.zoo.get_enclosures.return_value = [enclosure]

        self.image_recognition_system.recognize_animal_status.return_value = animal_status
        self.image_recognition_system.recognize_enclosure_status.return_value = enclosure_status

        self.zoo_inspector.inspect(self.zoo)

        self.zoo.request_maintenance_crew_to.assert_not_called()
        self.zoo.request_security_to.assert_not_called()
        self.zoo.request_veterinary_to.assert_not_called()
        self.zoo.close_enclosure.assert_not_called()

        self.verify_log(f'ZOO#{self.zoo_id}#OK')

    def test_when_enclosure_is_not_safe_then_requests(self):
        animal = Mock()

        enclosure = Mock()
        enclosure.get_animal.return_value = animal
        enclosure.get_id.return_value = self.test_enclosure_id

        animal_status = Mock()
        animal_status.is_animal_sick.return_value = False

        enclosure_status = Mock()
        enclosure_status.is_enclosure_safe.return_value = False

        self.zoo.get_enclosures.return_value = [enclosure]

        self.image_recognition_system.recognize_animal_status.return_value = animal_status
        self.image_recognition_system.recognize_enclosure_status.return_value = enclosure_status

        self.zoo_inspector.inspect(self.zoo)

        self.zoo.request_maintenance_crew_to.assert_called_once_with(ANY)
        self.zoo.request_security_to.assert_called_once_with(ANY)
        self.zoo.close_enclosure.assert_called_once_with(ANY)

        # but animal is safe
        self.zoo.request_veterinary_to.assert_not_called()

        self.verify_log(f'ENCLOSURE#{self.test_enclosure_id}#WARNING', f'ZOO#{self.zoo_id}#WARNING')

    def test_when_animal_is_sick_then_requests(self):
        animal = Mock()
        animal.get_name.return_value = self.test_animal_name

        enclosure = Mock()
        enclosure.get_animal.return_value = animal

        animal_status = Mock()
        animal_status.is_animal_sick.return_value = True

        enclosure_status = Mock()
        enclosure_status.is_enclosure_safe.return_value = True

        self.zoo.get_enclosures.return_value = [enclosure]

        self.image_recognition_system.recognize_animal_status.return_value = animal_status
        self.image_recognition_system.recognize_enclosure_status.return_value = enclosure_status

        self.zoo_inspector.inspect(self.zoo)

        self.zoo.close_enclosure.assert_called_once_with(ANY)
        self.zoo.request_veterinary_to.assert_called_once_with(ANY)
        # but enclosure is fine
        self.zoo.request_maintenance_crew_to.assert_not_called()
        self.zoo.request_security_to.assert_not_called()

        self.verify_log(f'ANIMAL#{self.test_animal_name}#WARNING', f'ZOO#{self.zoo_id}#WARNING')

    def test_when_first_animal_sick_second_healthy_still_request(self):
        sick_animal = Mock()
        sick_animal.get_name.return_value = 'SICK_ANIMAL'

        healthy_animal = Mock()
        healthy_animal.get_name.return_value = 'HEALTHY_ANIMAL'

        enclosure_with_sick_animal = Mock()
        enclosure_with_sick_animal.get_animal.return_value = sick_animal

        enclosure_with_healthy_animal = Mock()
        enclosure_with_healthy_animal.get_animal.return_value = healthy_animal

        sick_animal_status = Mock()
        sick_animal_status.is_animal_sick.return_value = True

        healthy_animal_status = Mock()
        healthy_animal_status.is_animal_sick.return_value = False

        animal_status = {
            sick_animal.get_name(): sick_animal_status,
            healthy_animal.get_name(): healthy_animal_status
        }

        enclosure_status = Mock()
        enclosure_status.is_enclosure_safe.return_value = True

        self.zoo.get_enclosures.return_value = [enclosure_with_sick_animal, enclosure_with_healthy_animal]

        self.image_recognition_system.recognize_animal_status = lambda animal, dummy: animal_status[animal.get_name()]

        self.image_recognition_system.recognize_enclosure_status.return_value = enclosure_status

        self.zoo_inspector.inspect(self.zoo)

        self.zoo.close_enclosure.assert_called_once_with(enclosure_with_sick_animal)
        self.zoo.request_veterinary_to.assert_called_once_with(sick_animal)
        #  but enclosure is fine
        self.zoo.request_maintenance_crew_to.assert_not_called()
        self.zoo.request_security_to.assert_not_called()

        self.verify_log('ANIMAL#SICK_ANIMAL#WARNING', f'ZOO#{self.zoo_id}#WARNING')
